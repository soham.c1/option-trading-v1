from django.db import models
import uuid
from datetime import datetime
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


# Create your models here.

##################### Table storing index nomenclature #####################
class OptionIndex(models.Model):
    index_uuid = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    index = models.CharField(max_length=100, default="Unknown", blank=False, null=False)
    index_symbol = models.CharField(max_length=100, default="Unknown", blank=False, null=False)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return f"{self.index}"

##################### Table storing index live update/state #####################
class IndexState(models.Model):
    index = models.ForeignKey(OptionIndex, on_delete=models.CASCADE, blank=False, null=False)
    date = models.DateField(default=datetime.now)
    open = models.IntegerField(default=0, blank=False, null=False)
    close = models.IntegerField(default=0, blank=False, null=False)
    high = models.IntegerField(default=0, blank=False, null=False)
    low = models.IntegerField(default=0, blank=False, null=False)
    variation = models.IntegerField(default=0, blank=False, null=False)
    year_high = models.IntegerField(default=0, blank=False, null=False)
    year_low = models.IntegerField(default=0, blank=False, null=False)
    price_to_book = models.IntegerField(default=0, blank=False, null=False)
    price_to_earning = models.IntegerField(default=0, blank=False, null=False)
    dividend_yield = models.IntegerField(default=0, blank=False, null=False)
    declines = models.IntegerField(default=0, blank=False, null=False)
    advances = models.IntegerField(default=0, blank=False, null=False)
    unchanged = models.IntegerField(default=0, blank=False, null=False)

    def __str__(self) -> str:
        return f"{self.index.index_symbol}__{self.date}"

    class Meta:
        unique_together = ('index', 'date')

##################### Table storing live values for each index #####################
class IndexValue(models.Model):
    index = models.ForeignKey(OptionIndex, on_delete=models.CASCADE, blank=False, null=False)
    date = models.DateTimeField(default=datetime.now)
    underlying_value = models.IntegerField(default=0, blank=False, null=False)
    CE_total_open_interest = models.IntegerField(default=0, blank=False, null=False)
    CE_total_volume = models.IntegerField(default=0, blank=False, null=False)
    PE_total_open_interest = models.IntegerField(default=0, blank=False, null=False)
    PE_total_volume = models.IntegerField(default=0, blank=False, null=False)
    
    def __str__(self) -> str:
        return f"{self.index.index_symbol}_{self.date}_{self.underlying_value}"

    class Meta:
        unique_together = ('index', 'date')

##################### Table storing current data for each index #####################
class IndexData(models.Model):
    index = models.ForeignKey(OptionIndex, on_delete=models.CASCADE, blank=False, null=False)
    expiry_date = models.DateField(default=datetime.now)
    strike_price = models.IntegerField(default=0, blank=False, null=False)

    def __str__(self) -> str:
        return f"{self.index.index_symbol}_{self.expiry_date}_{self.strike_price}"

def validate_option_contract(value):
    if value.upper() not in ["PE", "CE"]:
        raise ValidationError(
            _('%(value)s is not an acceptable Option Contract'),
            params={'value': value},
        )


##################### Table storing main option data #####################
# This table stores the data for each contract per index per strike price per exiry date
# Note : This is the most important and largest table
class OptionData(models.Model):
    index = models.ForeignKey(OptionIndex, on_delete=models.CASCADE, blank=False, null=False)
    index_data = models.ForeignKey(IndexData, on_delete=models.CASCADE, blank=False, null=False)
    contract = models.CharField(max_length=2, validators=[validate_option_contract])
    open_interest = models.IntegerField(default=0, blank=False, null=False)
    open_interest_change = models.IntegerField(default=0, blank=False, null=False)
    open_interest_pchange = models.IntegerField(default=0, blank=False, null=False)
    total_traded_volume = models.IntegerField(default=0, blank=False, null=False)
    implied_volatility = models.IntegerField(default=0, blank=False, null=False)
    last_price = models.IntegerField(default=0, blank=False, null=False)
    change = models.IntegerField(default=0, blank=False, null=False)
    pchange = models.IntegerField(default=0, blank=False, null=False)
    total_buy_quantity = models.IntegerField(default=0, blank=False, null=False)
    total_sell_quantity = models.IntegerField(default=0, blank=False, null=False)
    total_sell_quantity = models.IntegerField(default=0, blank=False, null=False)
    bid_quantity = models.IntegerField(default=0, blank=False, null=False)
    bid_price = models.IntegerField(default=0, blank=False, null=False)
    ask_quantity = models.IntegerField(default=0, blank=False, null=False)
    ask_price = models.IntegerField(default=0, blank=False, null=False)

    def __str__(self) -> str:
        return f"{self.index.index_symbol}{self.index_data.expiry_date}{self.contract}{self.index_data.strike_price}"
    
    class Meta:
        unique_together = ('index', 'index_data', 'contract')