FROM python:3.10.1-alpine3.15

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

WORKDIR /nse_project

ADD ./requirements.txt .

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN apk add build-base
RUN apk add --update --no-cache py-pip
RUN apk add --no-cache libffi-dev
RUN apk add --no-cache postgresql-dev
RUN apk add --no-cache openssl-dev
RUN apk add --no-cache libc-dev
RUN apk add --no-cache rust
RUN apk add --no-cache cargo
RUN apk add --no-cache make
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN pip3 install -r requirements.txt

ADD ./ .